from django.shortcuts import render, redirect
from .models import Urls
from django.views.decorators.csrf import csrf_exempt


def get_counter():
    counter = 1
    ok = False
    if Urls.objects.count() != 0:
        while not ok:
            search_url = Urls.objects.filter(short_url=str(counter))
            if search_url:
                counter += 1
            else:
                ok = True
    return counter


@csrf_exempt
def index(request):
    if request.method == "GET":
        url_list = Urls.objects.all()
        return render(request, "acortadora/page.html", {'url_list': url_list})
    elif request.method == "POST":
        url_list = Urls.objects.all()
        new_url = request.POST["url"]
        url = Urls.objects.filter(url__endswith=new_url)
        if not url:
            if 'http://' not in new_url and 'https://' not in new_url:
                new_url = 'http://' + new_url
            if not request.POST["short_url"]:
                counter = get_counter()
                url = Urls(short_url=str(counter), url=new_url)
                url.save()
            else:
                url = Urls(short_url=request.POST["short_url"], url=new_url)
                url.save()
        return render(request, "acortadora/page.html", {'url_list': url_list})


def get(request, resource):
    url_list = Urls.objects.all()
    try:
        short_url = Urls.objects.get(short_url=resource)
        return redirect(short_url.url)
    except Urls.DoesNotExist:
        return render(request, "acortadora/page_not_found.html", {'resource': resource, 'url_list': url_list})
