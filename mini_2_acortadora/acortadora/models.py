from django.db import models


class Urls(models.Model):
    # Parameters of the Short Url
    url = models.TextField()
    short_url = models.CharField(max_length=256)

